var expect = require("chai").expect;
var http = require('http');
var randomstring = require('randomstring');
var jf = require('jsonfile');
var redis = require('redis');
var client = redis.createClient();

describe("listens only on route /track", function() 
{
	it("listens on route /track", function() {
		var options = {
			host: '127.0.0.1:8080',
			path: '/track'
		};

		callback = function(response) {
			var str = '';

			//another chunk of data has been recieved, so append it to `str`
			response.on('data', function (chunk) {
				str += chunk;
			});

			//the whole response has been recieved, so we just print it out here
			response.on('end', function () {
				expect(str).to.equal("<h1>Processing...<h1>");
			});
			done();
		}
		http.request(options, callback).end();
	});
	it("doesn't listen outside the route /track", function() {
		var options = {
			host: '127.0.0.1:8080',
			path: '/'+randomstring.generate({
							length: 7,
							charset: 'alphabetic'
						})
		};

		callback = function(response) {
			var str = '';

			//another chunk of data has been recieved, so append it to `str`
			response.on('data', function (chunk) {
				str += chunk;
			});

			//the whole response has been recieved, so we just print it out here
			response.on('end', function () {
				expect(str).to.equal("<h1>Error!<h1>");
			});
			done();
		}
		http.request(options, callback).end();
	});
});
describe("writes data to json", function() 
{
	it("writes data to json", function() {
		var randomQuery = new Object();
		randomQuery[randomstring.generate({
								length: 7,
								charset: 'alphabetic'
							})] =
		randomstring.generate({
							length: 7,
							charset: 'alphabetic'
						});
		var options = {
			host: '127.0.0.1:8080',
			path: '/track?'+ JSON.stringify(randomQuery).replace(":","=")
		};

		var ocekavanyJSON;
		jf.readFile("data.json", function(err, obj) {
			var ocekavanyJSON = absorb(obj,randomQuery,true);
			if(err != null)
				console.log(err);
			done();
		});
		callback = function(response) {
			var str = '';

			//another chunk of data has been recieved, so append it to `str`
			response.on('data', function (chunk) {
				str += chunk;
			});

			//the whole response has been recieved, so we just print it out here
			response.on('end', function () {
				expect(str).to.equal("<h1>Processing...<h1>");
			});
			done();
		}
		http.request(options, callback).end();
		jf.readFile("data.json", function(err, obj) {
			obj;
			if(err != null)
				console.log(err);
			expect(obj).to.equal(ocekavanyJSON);
			done();
		});
	});
});
describe("sets count in redis", function() 
{
	it("sets the count if it is a number", function() {
		var cislo = Math.random();
		var options = {
			host: '127.0.0.1:8080',
			path: '/track?'+ "count="+cislo
		};

		var ocekavanyCount;
		client.get('count', function(err, reply) {
			ocekavanyCount = parseInt(reply)+cislo;
		    if(err != null)
				console.log(err);
			done();
		});
		callback = function(response) {
			var str = '';

			//another chunk of data has been recieved, so append it to `str`
			response.on('data', function (chunk) {
				str += chunk;
			});

			//the whole response has been recieved, so we just print it out here
			response.on('end', function () {
				expect(str).to.equal("<h1>Processing...<h1>");
			});
			done();
		}
		http.request(options, callback).end();
		client.get('count', function(err, reply) {
			expect(reply).to.equal(ocekavanyCount);
		    if(err != null)
				console.log(err);
			done();
		});
	});
	it("doesn't set the count if it isn't a number", function() {
		var options = {
			host: '127.0.0.1:8080',
			path: '/track?'+ "count="+randomstring.generate({
							length: 7,
							charset: 'alphabetic'
						})
		};

		var ocekavanyCount;
		client.get('count', function(err, reply) {
			ocekavanyCount = parseInt(reply);
		    if(err != null)
				console.log(err);
			done();
		});
		callback = function(response) {
			var str = '';

			//another chunk of data has been recieved, so append it to `str`
			response.on('data', function (chunk) {
				str += chunk;
			});

			//the whole response has been recieved, so we just print it out here
			response.on('end', function () {
				expect(str).to.equal("<h1>Processing...<h1>");
			});
			done();
		}
		http.request(options, callback).end();
		client.get('count', function(err, reply) {
			expect(reply).to.equal(ocekavanyCount);
		    if(err != null)
				console.log(err);
			done();
		});
	});
});