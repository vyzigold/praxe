var url2 = require('url');
var absorb = require('absorb');
var jf = require('jsonfile');    
var http = require("http");
var redis = require('redis');
var client = redis.createClient();
var file = 'data.json';
var server = http.createServer(function(request, response) {
	response.writeHead(200, {"Content-Type": "text/html"});
	var url = url2.parse(request.url,true);
	console.log(url.pathname);
	if(url.pathname === '/track')
	{
		response.write("<h1>Processing...</h1>");
		if(typeof url.query.count !== 'undefined')
		{
			var count = url.query.count;
			delete url.query.count;
			if(!isNaN(count))
			{
				client.get('count', function(err, reply) {
					if(!isNaN(reply))
						client.set('count', parseInt(reply)+parseInt(count), function(err, reply) {
						if(err != null)
							console.log(err);
					});
					else
						client.set('count', parseInt(count), function(err, reply) {
						if(err != null)
							console.log(err);
					});
				    if(err != null)
						console.log(err);
				});
			}
		}
		jf.readFile(file, function(err, obj) {
			var novy = absorb(obj,url.query,true);
			if(err != null)
				console.log(err);
        	jf.writeFile(file, novy, function(err, obj) {
				var novy = absorb(obj,url.query,true);
				if(err != null)
					console.log(err);
			})
		});
	}
	else
		response.write("<h1>Error!</h1>");
	response.end();
});
server.listen(8080);